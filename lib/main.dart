import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Lifestyle_products/list_page.dart';
import 'package:Lifestyle_products/pages/main_drawer.dart';
import 'package:flutter/services.dart';

void main() =>
    runApp(MaterialApp(debugShowCheckedModeBanner: false, home: HomePage()));

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final List<String> _listItem = [
    'assets/images/two.jpg',
    'assets/images/three.jpg',
    'assets/images/four.jpg',
    'assets/images/five.jpg',
    'assets/images/two.jpg',
    'assets/images/three.jpg',
    'assets/images/four.jpg',
    'assets/images/five.jpg',
  ];
  final List<String> _listItem1 = [
    'assets/images/six.jpg',
    'assets/images/seven.jpg',
    'assets/images/eight.jpg',
    'assets/images/nine.jpg',
    'assets/images/ten.jpg',
    'assets/images/eleven.jpg',
    'assets/images/twelve.jpg',
    'assets/images/thirteen.jpg',
  ];

  Widget header() {
    return Container(
        width: double.infinity,
        height: 250,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
                image: AssetImage('assets/images/one.jpg'), fit: BoxFit.cover)),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(begin: Alignment.bottomRight, colors: [
                Colors.black.withOpacity(.8),
                Colors.black.withOpacity(.5),
              ])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Home Furniture Sale",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ));
  }

  Widget horizontalListView() {
    return SliverToBoxAdapter(
      child: Container(
        height: 150,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _listItem.length,
          itemBuilder: (BuildContext context, int index) => Card(
            color: Colors.transparent,
            elevation: 0,
            child: Container(
              width: 140,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: AssetImage(_listItem1[index]), fit: BoxFit.cover)),
            ),
          ),
        ),
      ),
    );
  }

  Card buildCard(index) {
    return Card(
      color: Colors.transparent,
      elevation: 0,
      child: Container(
        width: 180,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
                image: AssetImage(_listItem[index]), fit: BoxFit.cover)),
      ),
    );
  }
  _showPopupMenu(){
    showMenu<String>(
      context: context,
      position: RelativeRect.fromLTRB(25.0, 25.0, 0.0, 0.0),      //position where you want to show the menu on screen
      items: [
        PopupMenuItem<String>(
            child: const Text('Login'), value: '1'),
        PopupMenuItem<String>(
            child: const Text('Sign up'), value: '2'),
        PopupMenuItem<String>(
            child: const Text('Exit '), value: '3'),
      ],
      elevation: 8.0,
    )
        .then<void>((String itemSelected) {

      if (itemSelected == null) return;

      if(itemSelected == "1"){
        //code here
      }else if(itemSelected == "2"){
        //code here
      }else if(itemSelected == "3"){
        SystemNavigator.pop();
        //code here
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(.9),
      drawer: MainDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.black.withOpacity(.8),
        elevation: 0,
        // leading: Icon(Icons.menu),
        title: Text("Home Decor"),
        actions: <Widget>[
          Padding(padding: EdgeInsets.fromLTRB(0, 0,0,0),),
          IconButton(icon: Icon(
            Icons.more_vert,
          ),
            onPressed: (){
              _showPopupMenu();

            },
          )

        ],
          ),

      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            header(),
            SizedBox(
              height: 20,
            ),
            Expanded(
                child: CustomScrollView(
              slivers: [
                horizontalListView(),
                SliverGrid(
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1,
                  ),
                  delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    return buildCard(index);
                  }, childCount: _listItem.length),
                ),
                horizontalListView(),
              ],
            )),
          ],
        ),
      ),
    );
  }
}

class ButttonList extends StatelessWidget {
  final String title;
  final Function onPressed;

  const ButttonList({Key key, @required this.title, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      color: Color(
        0xFFFFFFFF,
      ),
      child: Text(
        "$title",
        style: TextStyle(color: Colors.black),
      ),
    );
  }
}
