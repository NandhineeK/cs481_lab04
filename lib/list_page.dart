import 'package:flutter/material.dart';
import 'package:Lifestyle_products/pages/homepages.dart';

class ListPage extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: new HomePages(),

    );
  }
}