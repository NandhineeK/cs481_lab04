class Product {

  //--- Name Of Product
  final String name;
  //-- image
  final String image;
  //--- pricerange
  final String pricerange;
  //--- number_count
  final String number_count;

  Product({this.name,this.number_count,this.pricerange,this.image});

  static List<Product> allProducts()
  {
    var lstOfProducts = new List<Product>();

    lstOfProducts.add(new Product(name:"Velvet Chair" ,number_count: "Availability: 147" ,pricerange: "100 - 500 dollars" ,image: "chair.jpg"));
    lstOfProducts.add(new Product(name:"Marble Dinning Table",number_count: "Availability: 98",pricerange: "300 - 800 dollars",image: "dinning_table.jpg"));
    lstOfProducts.add(new Product(name:"Baystorm Queen Bed",number_count: "Availability: 240",pricerange: "230 - 600 dollars",image: "bed.jpg"));
    lstOfProducts.add(new Product(name:"Comfortable Sofa",number_count: "Availability: 60",pricerange: "700 - 1600 dollars",image: "sofa.jpg"));
    lstOfProducts.add(new Product(name:"Bar Counter Stool",number_count: "Availability: 80",pricerange: "250 - 500 dollars",image: "Bar_counter_stool.jpg"));
    lstOfProducts.add(new Product(name:"Coffee Table",number_count: "Availability: 79",pricerange: "130 - 800 dollars",image: "coffee_table.jpg"));
    lstOfProducts.add(new Product(name:"Double Dresser",number_count: "Availability: 300",pricerange: "430 - 550 dollars",image: "Double-dresser.jpg"));
    lstOfProducts.add(new Product(name:"Book Rack Display Shelf",number_count: "Availability: 240",pricerange: "300 - 750 dollars",image: "Book-Rack-Display-Shelf.jpg"));


    return lstOfProducts;
  }
}