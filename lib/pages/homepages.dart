import 'package:flutter/material.dart';
import 'package:Lifestyle_products/model/product.dart';
import 'package:Lifestyle_products/pages/main_drawer.dart';


class HomePages extends StatelessWidget {
  final List<Product> _allProducts = Product.allProducts();

  HomePages() {}

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            "List of Products",
            style: new TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ),
        drawer:MainDrawer(),
        body: new Padding(
            padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
            child: getHomePageBody(context)));
  }

  getHomePageBody(BuildContext context) {
    return ListView.builder(
      itemCount: _allProducts.length,
      itemBuilder: _getItemUI,
      padding: EdgeInsets.all(0.0),

    );
  }

  // First Task
/* Widget _getItemUI(BuildContext context, int index) {
   return new Text(_allProducts[index].name);
 }*/

  Widget _getItemUI(BuildContext context, int index) {
    return new Card(
        child: new Column(

          children: <Widget>[
            new ListTile(
              leading: new Image.asset(
                "assets/images/" + _allProducts[index].image,
                fit: BoxFit.fitHeight,
                width: 100.0,
              ),

              title: new Text(
                _allProducts[index].name,
                style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              subtitle: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text("\n",
                        style: new TextStyle(
                            fontSize: 6.0, fontWeight: FontWeight.normal)),
                    new Text(_allProducts[index].number_count,
                        style: new TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.normal)),
                    new Text('Price Range: ${_allProducts[index].pricerange}',
                        style: new TextStyle(
                            fontSize: 13.0, fontWeight: FontWeight.normal)),
                    new Text("\n",
                        style: new TextStyle(
                            fontSize: 6.0, fontWeight: FontWeight.normal)),
                  ]),

              onTap: () {
                _showSnackBar(context, _allProducts[index]);
              },
            )
          ],
        ));
  }

  _showSnackBar(BuildContext context, Product item) {
    final SnackBar objSnackbar = new SnackBar(
      content: new Text("${item.name} is a Product in ${item.number_count}"),
      backgroundColor: Colors.black,
    );
    Scaffold.of(context).showSnackBar(objSnackbar);
  }
}