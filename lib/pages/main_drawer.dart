import 'package:flutter/material.dart';
import 'package:Lifestyle_products/list_page.dart';
import 'package:Lifestyle_products/pages/contactus.dart';
import '../main.dart';

class MainDrawer extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    return Drawer
      (
        child: new ListView(
          children: <Widget> [
            new DrawerHeader(child: new Text('Side Menu',
              style:TextStyle(color: Colors.white, fontSize: 25),),
              decoration: BoxDecoration(
                  color: Colors.green,
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('assets/images/side2.jpg'))),
            ),
            new ListTile(
              leading: Icon(Icons.home),
              title: new Text('Welcome'),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => new HomePage()));
              },
            ),
            new ListTile(
              leading: Icon(Icons.info),
              title: new Text('Product List'),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => new ListPage()));
              },
            ),
            new Divider(),
            new ListTile(
              leading: Icon(Icons.contact_phone),
              title: new Text('ContactUs'),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => new Contact()));
              },
            ),
          ],
        ),
    );
  }
}