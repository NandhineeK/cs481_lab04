import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Lifestyle_products/list_page.dart';
import 'package:Lifestyle_products/pages/main_drawer.dart';

class Contact extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: AppBar(
        backgroundColor: Colors.black.withOpacity(.8),
        title: new Text("Contact Us",
      style: new TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          color: Colors.white),
      ),
      ),
      drawer:MainDrawer(),
      body: ListView(
        children: [
          Image.asset('assets/images/five.jpg',
            fit: BoxFit.cover,
            height:300,
            width:100,
          ),
          Padding(
            padding: EdgeInsets.only(top:55),
            child:Text("Home Decor",textAlign: TextAlign.center,
              style: new TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.pinkAccent),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left:100,top: 30 ),
            child:Row(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.call,
                  color: Colors.lightBlueAccent,),
                Text(" 1234 1234",textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Colors.white),
                ),
              ],
            ),
          ),
       Padding(
         padding: EdgeInsets.only(left:100,top: 30 ),
         child:Row(
           //crossAxisAlignment: CrossAxisAlignment.center,
             children: <Widget>[
               Icon(Icons.home,
                 color: Colors.lightBlueAccent,),
               Text(" SanDiego, California",textAlign: TextAlign.center,
                 style: new TextStyle(
                     fontSize: 20.0,
                     fontWeight: FontWeight.w300,
                     color: Colors.white),
               ),
             ],
         ),
       ),
        ],
      ),
    );
  }
}

